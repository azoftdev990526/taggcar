import { Component } from '@angular/core';

import { FindRidePage } from '../findride/findride';
import { MyridePage } from '../myride/myride';
import { fromPage } from '../from/from';

@Component({
  templateUrl: 'start.html'
})
export class StartPage {

  tab1Root = FindRidePage;
  tab2Root = MyridePage;
  tab3Root = fromPage;

  constructor() {

  }
}
