import { Component, Inject } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import {Camera,CameraOptions} from '@ionic-native/camera/ngx';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { a } from '@angular/core/src/render3';
import { IdentifyphotoPage } from '../ua_identifyimage/identifyimage';

@Component({
  selector: 'page-uahome',
  templateUrl: 'ua_home.html'
})
export class UAHomePage {
  base64img:string='';
  constructor(@Inject(APP_CONFIG) private config: AppConfig,  public nav: NavController,private platform: Platform,private camera:Camera) {
  }

  imageCaptured(){
    const options:CameraOptions={
      quality:70,
      destinationType:this.camera.DestinationType.DATA_URL,
      encodingType:this.camera.EncodingType.JPEG,
      mediaType:this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((ImageData=>{
       this.base64img="data:image/jpeg;base64,"+ImageData;
    }),error=>{
      console.log(error);
    })
  }

  imageCapturedGallery(){
    const options:CameraOptions={
      quality:70,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false
    }
    this.camera.getPicture(options).then((ImageData=>{
       this.base64img="data:image/jpeg;base64,"+ImageData;
    }),error=>{
      console.log(error);
    })
  }
  nextPage(){
    this.config.base64img = this.base64img;
    this.nav.push(IdentifyphotoPage);
  }
  clear(){
    this.base64img='';
  }
}