import { RiderprofilePage } from '../riderprofile/riderprofile';
import { NotificationsPage } from '../notifications/notifications';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Tripastep5Page } from '../tripastep5/tripastep5';


@Component({
  selector: 'page-tripastep4',
  templateUrl: 'tripastep4.html'
})
export class Tripastep4Page {

  leavetime:string;
  constructor(@Inject(APP_CONFIG) private config: AppConfig,public navCtrl: NavController) {
    this.leavetime = "08:30";
  }
  
 continuestep5(leavetime) {
   this.config.driverData.leave_time = this.leavetime;
   this.navCtrl.push(Tripastep5Page);
 }

  profile() {
    this.navCtrl.push(RiderprofilePage);
  }
  chats() {
    this.navCtrl.push(NotificationsPage);
  }

}