import { Component, Inject } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { HttpClient } from '@angular/common/http';
import { RiderprofilePage } from '../riderprofile/riderprofile';
import { NotificationsPage } from '../notifications/notifications';
import { Constants } from "../../models/constants.models";
import { MyridePage } from '../myride/myride';

@Component({
  selector: 'page-tripastep',
  templateUrl: 'tripastep.html'
})
export class TripastepPage {

  loading: any;
  loadingShown: boolean;
  comment: string;
  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController, private loadingCtrl: LoadingController, private toastCtrl: ToastController, private http: HttpClient) {

  }

  presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.onDidDismiss(() => { });
    this.loading.present();
    this.loadingShown = true;
  }
  dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }
  showToast(message: string) {
    let toast = this.toastCtrl.create({
        message: message,
        duration: 1500,
        position: 'bottom'
    });
    toast.onDidDismiss(() => {
        console.log('Dismissed toast');
    });
    toast.present();
  }   

  confirm() {
    console.log(this.config.driverData);
    this.config.driverData.comment = this.comment;
    this.presentLoading('Adding a trip...');
    this.http.post(Constants.API_URL + "/addtrip", this.config.driverData).subscribe((res) => {
      this.dismissLoading();
      if (res['status']==1) {
        if (this.config.LoggedId){
         // this.navCtrl.setRoot(StartPage);
          //this.navCtrl.push(StartPage);
         this.navCtrl.popToRoot();
        try{ 
           MyridePage.Instance.ngOnInit();
        }catch(e){

        }
        }
        else{ 
         // this.navCtrl.setRoot(MainPage);
          //this.navCtrl.push(StartPage);
        }
      } else {
        this.showToast(res['msg']);
      }
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
      this.dismissLoading();
    });
  }
  profile() {
    this.navCtrl.push(RiderprofilePage);
  }
  chats() {
    this.navCtrl.push(NotificationsPage);
  }
}