import { Component, Inject, NgZone} from '@angular/core';
import { NavController } from 'ionic-angular';
import { LocationPage } from '../location/location';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Geolocation } from '@ionic-native/geolocation';
import {ViewController} from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Searchbar } from 'ionic-angular';

declare var google: any;

@Component({
  selector: 'to-where',
  templateUrl: 'to.html'
})
export class toPage {
  autocompleteItems;
  autocomplete;

  latitude: number = 0;
  longitude: number = 0;
  geo: any

  service = new google.maps.places.AutocompleteService();
  showList: boolean = false;

  @ViewChild('searchText') searchText: Searchbar;
  constructor (@Inject(APP_CONFIG) private config: AppConfig,public viewCtrl: ViewController, private zone: NgZone,  public navCtrl: NavController, public geolocation: Geolocation) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {
    this.viewCtrl.dismiss(item);
    this.geo = item;
    this.geoCode(this.geo);//convert Address to lat and long
  }

  updateSearch() {
    this.showList = true;

    if (this.autocomplete.query == '') {
     this.autocompleteItems = [];
     return;
    }

    let me = this;
    this.service.getPlacePredictions({
    input: this.autocomplete.query,
    componentRestrictions: {
      //country: 'de'
    }
   }, (predictions, status) => {
     me.autocompleteItems = [];

   me.zone.run(() => {
     if (predictions != null) {
        predictions.forEach((prediction) => {
          me.autocompleteItems.push(prediction.description);
        });
       }
     });
   });
  }

  //convert Address string to lat and long
  geoCode(address:any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
    this.latitude = results[0].geometry.location.lat();
    this.longitude = results[0].geometry.location.lng();
    //alert("lat: " + this.latitude + ", long: " + this.longitude);
   });
 }

  selectItem(item) {
    this.autocomplete.query = item;
    this.showList = false;
    // this.from = item.location;
    // this.from_cityName = item.city_name;
    // this.from_cityId = item.city_id;
    // this.from_placeId = item.id;
    // this.from_lat = item.lat;
    // this.from_lng = item.lng;
    // this.showList = false;
  }

  continue() {
    // if (this.from_cityId>0) {
      this.config.driverData.arrival = this.autocomplete.query;

      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': this.autocomplete.query }, (results, status) => {
        this.latitude = results[0].geometry.location.lat();
        this.longitude = results[0].geometry.location.lng();
        console.log("lat: " + this.latitude + ", long: " + this.longitude);

        this.config.driverData.arrival_latlng = {
          lat : this.latitude,
          lng : this.longitude,
        }
        this.navCtrl.push(LocationPage);
      });
      // this.config.driverData.path_detail = this.path_detail+'-'+this.to_cityId+'_'+this.to_placeId;
      
    // }
  }

  currentlocation() {
    var thisFromPage = this;

    this.geolocation.getCurrentPosition().then((resp) => {
      let pos = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      };
      new google.maps.Geocoder().geocode({'location' : pos}, function(results, status) {
        //console.log(results[0].formatted_address);
        thisFromPage.setSearchbarText(results[0].formatted_address);
      }); 
    });
    //this.navCtrl.push(LocationPage);

    // var result = this.geolocation.getCurrentPosition(
    //   {maximumAge: 1000, timeout: 5000,
    //    enableHighAccuracy: true });
    // console.log(result);
      // ).then((resp) => {
      //       // resp.coords.latitude
      //       // resp.coords.longitude
      //       //alert("r succ"+resp.coords.latitude)
      //       alert(JSON.stringify( resp.coords));
      
      //       // this.lat=resp.coords.latitude
      //       // this.lng=resp.coords.longitude
      //       },er=>{
      //         //alert("error getting location")
      //         alert('Can not retrieve Location')
      //       }).catch((error) => {
      //       //alert('Error getting location'+JSON.stringify(error));
      //       alert('Error getting location - '+JSON.stringify(error))
      //       });
  }

  setSearchbarText(text){
    this.searchText.value = text;
    this.autocomplete.query = text;
    this.showList = false;
  }
}
//     to: string;
//     to_cityName: string;
//     to_cityId: number;
//     to_placeId: number;
//     to_lat: number;
//     to_lng: number;
//     path_detail: string;
//     items: any;
//     showList:boolean = false;

//     constructor(@Inject(APP_CONFIG) private config: AppConfig, private http: HttpClient, navParams: NavParams,public navCtrl: NavController) {
//       this.path_detail = this.config.driverData.path_detail;
//     }
//     searchItems(searchTxt) {
//       this.showList = false;
//       this.http.get(Constants.API_URL + "/getLocation?keyword="+searchTxt).subscribe((res) => {
//         this.items = res;
//         this.showList = true;
//       }, (err) => {
//         console.log("Error Network:", JSON.stringify(err));
//       });
//     }
//     getItems(ev: any) {
//       this.showList = false;
//       let val = ev.target.value;
//       if (val=='') {
//         this.items = [];
//       } else {
//         this.searchItems(val);
//       }    
//     }
//     selectItem(item) {
//       this.to = item.location;
//       this.to_cityName = item.city_name;
//       this.to_cityId = item.city_id;
//       this.to_placeId = item.id;
//       this.to_lat = item.lat;
//       this.to_lng = item.lng;
//       this.showList = false;
//     }
//     continue() {
//       if (this.to_cityId>0) {
//         this.config.driverData.arrival = this.to_cityName;
//         this.config.driverData.path_detail = this.path_detail+'-'+this.to_cityId+'_'+this.to_placeId;
//         this.navCtrl.push(Tripastep3Page);
//       }
//     }
//     currentlocation(){
//       this.navCtrl.push(LocationPage);
//     }
//     profile() {
//       this.navCtrl.push(RiderprofilePage);
//     }
//     chats() {
//       this.navCtrl.push(NotificationsPage);
//     }
// }
