import { Component, Inject } from '@angular/core';
import { NavController,LoadingController , ToastController} from 'ionic-angular';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { SearchridePage } from '../searchride/searchride';
import { BookingPage } from '../booking/booking';
import { HttpClient } from '@angular/common/http';
import { Constants } from "../../models/constants.models";
import { MyridePage } from '../myride/myride';
import { LocationViewPage } from '../location_view/location_view';

@Component({
  selector: 'page-myridedetail',
  templateUrl: 'myride_detail.html'
})
export class MyRideDetailPage {

  item:any;
  detail:any;
  formated_date:any;
  nDate:number=1;
  nWeek:number=1;
  nMonth:number=1;
  nYear:number=2020;

  loading: any;
  loadingShown: boolean;

  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController, private http: HttpClient, private loadingCtrl: LoadingController, private toastCtrl:ToastController) {
    this.item = this.config.item;
    let date = new Date(this.item.start_date);
    this.nDate = date.getUTCDate();
    this.nWeek = date.getUTCDay();
    this.nMonth = date.getUTCMonth();
    this.nYear = date.getUTCFullYear();
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
        message: message,
        duration: 1500,
        position: 'bottom'
    });
    toast.onDidDismiss(() => {
        console.log('Dismissed toast');
    });
    toast.present();
  } 

  ngOnInit() {
    // this.presentLoading('Loading...');
    // let req = {
    //   "path_detail": this.item.path_detail,

    //   "from_cityId": this.config.findOptions.from_cityId, 
    //   "from_placeId": this.config.findOptions.from_placeId,
    //   "from_lat": this.config.findOptions.from_lat,
    //   "from_lng": this.config.findOptions.from_lng,

    //   "to_cityId": this.config.findOptions.to_cityId, 
    //   "to_placeId": this.config.findOptions.to_placeId,
    //   "to_lat": this.config.findOptions.to_lat,
    //   "to_lng": this.config.findOptions.to_lng,
    // }
    // this.http.post(Constants.API_URL + "/trip_detail", req).subscribe((res) => {
    //   this.dismissLoading();
    //     this.detail = res;
    //     console.log(this.detail);
    // }, (err) => {
    //   console.log("Error Network:", JSON.stringify(err));
    //   this.dismissLoading();
    // });
    console.log("~~item = ");
    console.log(this.item);
  }
  presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.onDidDismiss(() => { });
    this.loading.present();
    this.loadingShown = true;
  }

  dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }
  driver_profile(){
    this.navCtrl.push(SearchridePage);
  }
  contact_driver(){
    
  }
  cancelaride(){
    this.presentLoading('Loading...');
    let req = {
      "id" : this.item.id
    };
    console.log(this.item);
    this.http.post(Constants.API_URL + "/deletearide", req).subscribe((res) => {
      this.dismissLoading();
       // this.detail = res;
        //console.log(this.detail);
        this.navCtrl.pop();
        MyridePage.Instance.ngOnInit();
        this.showToast("Canceled a ride successfully!");
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
      this.dismissLoading();
    });
  }

  onDeparture(){
    this.config.driverData.arrival_latlng = {
      "lat": parseFloat(this.item.from_lat),
      "lng": parseFloat(this.item.from_lng),
    }
    this.config.driverData.departure = this.item.departure;
    this.navCtrl.push(LocationViewPage);
  }
  onArrival(){
    this.config.driverData.arrival_latlng = {
      "lat": parseFloat(this.item.to_lat),
      "lng": parseFloat(this.item.to_lng),
    }
    this.config.driverData.departure = this.item.arrival;
    this.navCtrl.push(LocationViewPage);
  }
}
