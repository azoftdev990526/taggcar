import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Component, Inject } from '@angular/core';
import { NavController,LoadingController,ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SignupPage } from '../signup/signup';
import { StartPage } from '../start/start';
import { NotificationsPage } from "../notifications/notifications";
import { Constants } from '../../models/constants.models';
import { ProfilePage } from '../profile/profile';
import { RiderprofilePage } from '../riderprofile/riderprofile';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loading: any;
  loadingShown: boolean;
  loginForm: FormGroup;
  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController, private formBuilder: FormBuilder,
    private http: HttpClient, private loadingCtrl: LoadingController, private toastCtrl: ToastController) {
  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(3), Validators.required])]
    });
  }
  presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.onDidDismiss(() => { });
    this.loading.present();
    this.loadingShown = true;
  }

  dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }
  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  login() {
    if (!this.loginForm.valid) {
      return this.showToast('Please provide your Login information.')
    }
    this.presentLoading('Logging in...')
    let req = {
      "email": this.loginForm.value.email,
      "password": this.loginForm.value.password
    }
    this.http.post(Constants.API_URL + "/login", req).subscribe((res) => {
      this.dismissLoading();
      if (res['status']==1) {
        this.config.LoggedId = res['info']['id'];
        this.config.driverData.driver_id = this.config.LoggedId;
        this.config.info = res['info'];
        this.config.info.avatar_url = Constants.API_URL + "/../assets/img/avatar/" +  this.config.info.avatar_url;
        // this.navCtrl.push(RiderprofilePage);
        this.navCtrl.push(StartPage);
      } else {
        this.showToast(res['msg']);
      }
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
      this.dismissLoading();
    });
  }
  signup(){
    this.navCtrl.push(SignupPage);
  }
  facebook(){
    this.navCtrl.push(SignupPage);
  }
  notification(){
    this.navCtrl.push(NotificationsPage);
  }

}
