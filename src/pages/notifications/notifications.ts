import { Component, Inject, OnInit } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';

import { ConfirmpopupPage } from '../confirmpopup/confirmpopup';
import { MessagePage } from '../message/message';
import { MychattingPage } from "../mychatting/mychatting";
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../../models/constants.models';
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html'
})
export class NotificationsPage implements OnInit{
  rideprofile: string = "about";
  userList: any;
  avatarPrefix : any;
  loading: any;
  loadingShown: boolean;
  unreads: any;
  unreads_list: any;
 
  static instance: NotificationsPage;

  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController, private loadingCtrl: LoadingController  ,private http: HttpClient ) {
    this.avatarPrefix = Constants.API_URL + "/../assets/img/avatar/";
    this.presentLoading('Loading...')
    
    NotificationsPage.instance = this;
  }
  
  ngOnInit(){
    this.http.post(Constants.API_URL + "/getAvailables", {
      "id" : this.config.LoggedId,
    }).subscribe((res) => {
      this.userList = res['res'];
      this.unreads = res['count_sum'];
      this.unreads_list = res['unreads'];

      for(let i=0;i<this.userList.length;i++){
        this.userList[i]['unreads'] = this.unreads_list[i];
      }

      console.log("~~~userlist");
      console.log(this.userList);
      this.dismissLoading();
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
      this.dismissLoading();
    });
  }
  
    confirmpopup(){
        this.navCtrl.push(ConfirmpopupPage);
    }
 
    message(){
      this.navCtrl.push(MessagePage);
    }

    mychatting(opp: any){
      this.config.opp = opp;
      this.navCtrl.push(MychattingPage);
    }

    presentLoading(message: string) {
      this.loading = this.loadingCtrl.create({
        content: message
      });
      this.loading.onDidDismiss(() => { });
      this.loading.present();
      this.loadingShown = true;
    }
  
    dismissLoading() {
      if (this.loadingShown) {
        this.loadingShown = false;
        this.loading.dismiss();
      }
    }
}