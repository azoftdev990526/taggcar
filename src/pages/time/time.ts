import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ChattingPage } from '../chatting/chatting';

@Component({
  selector: 'time-when',
  templateUrl: 'time.html'
})
export class timePage {

  constructor(public navCtrl: NavController) {

  }
  
     chatting(){
      this.navCtrl.push(ChattingPage);
    }

}
