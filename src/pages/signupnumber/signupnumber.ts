import { Component } from '@angular/core';
import { NavController,LoadingController,ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { NavParams } from 'ionic-angular';

import { Constants } from '../../models/constants.models';
import { StartPage } from '../start/start';

@Component({
  selector: 'page-signupnumber',
  templateUrl: 'signupnumber.html'
})
export class signupnumberPage {
    loading: any;
    loadingShown: boolean;
    navParams: NavParams;
    data: {
        phone: string,
        name: string,
        firstname: string,
        lastname: string,
        birthday: string,
        role: string,
        email: string,
        password: string,
    }
    constructor(navParams: NavParams,public navCtrl: NavController, private http: HttpClient, private loadingCtrl: LoadingController, private toastCtrl: ToastController) {
        this.navParams = navParams;
        this.data = this.navParams.get('data');
    }
    presentLoading(message: string) {
        this.loading = this.loadingCtrl.create({
          content: message
        });
        this.loading.onDidDismiss(() => { });
        this.loading.present();
        this.loadingShown = true;
    }
    dismissLoading() {
        if (this.loadingShown) {
          this.loadingShown = false;
          this.loading.dismiss();
        }
    }
    showToast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 1500,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }   
  
    continuenumber(phone){
        this.data.phone = phone;
        this.data.name = this.data.firstname + ' ' + this.data.lastname;
        let tmp = this.data.birthday.split('/');
        if (tmp.length>2) {
            this.data.birthday = tmp[2]+'-'+tmp[1]+'-'+tmp[0];
        }
        this.presentLoading('Signing up...');
        console.log("~~this.data = %O" , this.data);
        this.http.post(Constants.API_URL + "/signup", this.data).subscribe((res) => {
          this.dismissLoading();
          if (res['status']==1) {
            /*
            if(this.data.role == "D"){
                this.navCtrl.push(fromPage, {
                    driver_id:res['driver_id'][0]['id']
                });
              //  this.showToast("SignUp Successful!");
              }
            else*/
                this.navCtrl.setRoot(StartPage);

            this.showToast("SignUp Successful!");
          } else {
            this.showToast(res['errors']['email']);
          }
        }, (err) => {
          console.log("Error Network:", JSON.stringify(err));
          this.dismissLoading();
        });
    }

}
