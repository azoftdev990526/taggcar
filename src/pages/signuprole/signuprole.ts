import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';

import { signupemailPage } from '../signupemail/signupemail';

@Component({
  selector: 'signup-role',
  templateUrl: 'signuprole.html'
})
export class Signuprole {

    navParams: NavParams;
    role: string = "";
    constructor(public navCtrl: NavController) {

    }
  
    continue(){
        this.navCtrl.push(signupemailPage, {
          role: this.role
        });
    }
    
    driver(){
        this.role='D';
    }
    passenger(){
        this.role='P';
    }

}
