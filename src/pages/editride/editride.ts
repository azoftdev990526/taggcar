import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';


@Component({
  selector: 'page-editride',
  templateUrl: 'editride.html'
})
export class EditRidePage {
ride: string = "oneway";

  constructor(public navCtrl: NavController) {

  }
  
 tabs(){
    this.navCtrl.push(TabsPage);
 }
 

}
