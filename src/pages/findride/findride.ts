import { Component, Inject, NgZone } from '@angular/core';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { NavController, ModalController } from 'ionic-angular';
import { datePage } from '../date/date';
import { SearchPage } from '../search/search';
import { PassengersPage } from '../passengers/passengers';
import {NavParams} from "ionic-angular";
import { NotificationsPage } from '../notifications/notifications';
import { RiderprofilePage } from '../riderprofile/riderprofile';
import {ViewController} from 'ionic-angular';
import { Constants } from '../../models/constants.models';

declare var google:any;

@Component({
  selector: 'page-findride',
  templateUrl: 'findride.html'
})
export class FindRidePage {

  ride: string = "Upconing";
  from: string;
  to: string;
  pdate:string;
  pcount:number;
  avatarUrl: string = "";
  autocompleteItemsFrom; 
  autocompleteItemsTo;

  service = new google.maps.places.AutocompleteService();
  constructor(@Inject(APP_CONFIG) private config: AppConfig, public viewCtrl: ViewController, private zone: NgZone, public navCtrl: NavController, public modalCtrl: ModalController, navParams: NavParams) {
    this.from = this.config.findOptions.from;
    this.to = this.config.findOptions.to;
    this.pdate = this.config.findOptions.pdate;
    this.pcount = this.config.findOptions.pcount;

    // this.from = "";
    // this.to = "";
    this.autocompleteItemsFrom = [];
    this.autocompleteItemsTo = [];

    this.avatarUrl = this.config.info.avatar_url;
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

  fromwhere() {
    //this.navCtrl.push(fromnewPage);
  }
  towhere() {
    //this.navCtrl.push(tonewPage);
  }
  search() {
    //if (this.config.findOptions.from_cityId>0 && this.config.findOptions.to_cityId>0)
    this.config.findOptions.from = this.from;
    this.config.findOptions.to = this.to;
    this.config.findOptions.pcount = this.pcount;
      this.navCtrl.push(SearchPage);
  }
  passengers() {
    this.navCtrl.push(PassengersPage);
  }
  date() {
    this.navCtrl.push(datePage);
  }
  profile() {
    this.navCtrl.push(RiderprofilePage);
  }
  chats() {
    this.navCtrl.push(NotificationsPage);
  }
  swap() {
    this.config.findOptions.from= this.to;
    this.config.findOptions.to  = this.from;
    this.from= this.config.findOptions.from;
    this.to  = this.config.findOptions.to;
    console.log("Swaped!");
  }

  updateSearch() {
    // this.showList = true;

    if (this.from == '') {
     this.autocompleteItemsFrom = [];
     return;
    }

    let me = this;
    this.service.getPlacePredictions({
    input: this.from,
    componentRestrictions: {
      //country: 'de'
    }
   }, (predictions, status) => {
     me.autocompleteItemsFrom = [];

   me.zone.run(() => {
     if (predictions != null) {
        predictions.forEach((prediction) => {
          me.autocompleteItemsFrom.push(prediction.description);
        });
       }
     });
   });
  }

  updateSearchTo() {
    // this.showList = true;

    if (this.to == '') {
     this.autocompleteItemsTo = [];
     return;
    }

    let me = this;
    this.service.getPlacePredictions({
    input: this.to,
    componentRestrictions: {
      //country: 'de'
    }
   }, (predictions, status) => {
     me.autocompleteItemsTo = [];

   me.zone.run(() => {
     if (predictions != null) {
        predictions.forEach((prediction) => {
          me.autocompleteItemsTo.push(prediction.description);
        });
       }
     });
   });
  }

  selectFromItem(item: any) {
    this.from = item;
    this.autocompleteItemsFrom = [];
    this.config.findOptions.from = item;
    //this.viewCtrl.dismiss(item);
    // this.geo = item;
    // this.geoCode(this.geo);//convert Address to lat and long
  }

  selectToItem(item: any) {
    this.to = item;
    this.autocompleteItemsTo = [];
    this.config.findOptions.to = item;
    //this.viewCtrl.dismiss(item);
    // this.geo = item;
    // this.geoCode(this.geo);//convert Address to lat and long
  }
}

