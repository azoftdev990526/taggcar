import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { signupemailPage } from '../signupemail/signupemail';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
    navParams: NavParams
    data: {
        gender: string
    }
  constructor(public navCtrl: NavController) {

  }
  
    signup_role(){
        //this.navCtrl.push(Signuprole);
        this.navCtrl.push(signupemailPage, {
          role: "P"
        });
    }
    login() {
        this.navCtrl.push(LoginPage);
    }
    
}
