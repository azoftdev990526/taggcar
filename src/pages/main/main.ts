import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import {LoginPage} from '../login/login';

@Component({
  selector: 'page-main',
  templateUrl: 'main.html'
})
export class MainPage {

  constructor(public navCtrl: NavController) {

  }
  
    reviews() {
       
    }
    loginemail(){
        // this.navCtrl.push(SignupPage);
        this.navCtrl.push(LoginPage);
    }
    
    signup(){
        this.navCtrl.push(SignupPage);
    }

}
