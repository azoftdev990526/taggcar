import {Component, NgZone, Inject} from '@angular/core';
import {ViewController} from 'ionic-angular';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { NavController } from 'ionic-angular';
import { toPage } from '../to/to';
import { Geolocation } from '@ionic-native/geolocation';
import { ViewChild } from '@angular/core';
import { Searchbar } from 'ionic-angular';
import { Button } from 'ionic-angular';
import { RiderprofilePage } from '../riderprofile/riderprofile';
import { NotificationsPage } from '../notifications/notifications';

declare var google: any;

@Component({
  selector: 'from-where',
  templateUrl: 'from.html'
})

export class fromPage {
  autocompleteItems;
  autocomplete;

  latitude: number = 0;
  longitude: number = 0;
  geo: any

  service = new google.maps.places.AutocompleteService();
  showList: boolean = false;

  @ViewChild('searchText') searchText: Searchbar;
  @ViewChild('btnContinue') btnContinue: Button;

  constructor (@Inject(APP_CONFIG) public config: AppConfig,public viewCtrl: ViewController, private zone: NgZone,  public navCtrl: NavController, public geolocation: Geolocation) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {
    this.viewCtrl.dismiss(item);
    this.geo = item;
    this.geoCode(this.geo);//convert Address to lat and long
  }

  updateSearch() {
    this.showList = true;

    if (this.autocomplete.query == '') {
     this.autocompleteItems = [];
     return;
    }

    let me = this;
    this.service.getPlacePredictions({
    input: this.autocomplete.query,
    componentRestrictions: {
      //country: 'de'
    }
   }, (predictions, status) => {
     me.autocompleteItems = [];

   me.zone.run(() => {
     if (predictions != null) {
        predictions.forEach((prediction) => {
          me.autocompleteItems.push(prediction.description);
        });
       }
     });
   });
  }

  //convert Address string to lat and long
  geoCode(address:any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
    this.latitude = results[0].geometry.location.lat();
    this.longitude = results[0].geometry.location.lng();
    alert("lat: " + this.latitude + ", long: " + this.longitude);
   });
 }

  selectItem(item) {
    this.autocomplete.query = item;
    this.showList = false;
    // this.from = item.location;
    // this.from_cityName = item.city_name;
    // this.from_cityId = item.city_id;
    // this.from_placeId = item.id;
    // this.from_lat = item.lat;
    // this.from_lng = item.lng;
    // this.showList = false;
  }

  continue() {
    // if (this.from_cityId>0) {
    this.config.driverData.departure = this.autocomplete.query;
    //   this.config.driverData.path_detail = this.from_cityId+'_'+this.from_placeId;
    let geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': this.autocomplete.query }, (results, status) => {
        this.latitude = results[0].geometry.location.lat();
        this.longitude = results[0].geometry.location.lng();
        console.log("lat: " + this.latitude + ", long: " + this.longitude);

        this.config.driverData.departure_latlng = {
          lat : this.latitude,
          lng : this.longitude,
        }
        this.navCtrl.push(toPage);
      });
    // }
  }

  currentlocation() {
    var thisFromPage = this;

    this.geolocation.getCurrentPosition().then((resp) => {
      let pos = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      };
      new google.maps.Geocoder().geocode({'location' : pos}, function(results, status) {
        //console.log(results[0].formatted_address);
        thisFromPage.setSearchbarText(results[0].formatted_address);
      }); 
    });
    //this.navCtrl.push(LocationPage);

    // var result = this.geolocation.getCurrentPosition(
    //   {maximumAge: 1000, timeout: 5000,
    //    enableHighAccuracy: true });
    // console.log(result);
      // ).then((resp) => {
      //       // resp.coords.latitude
      //       // resp.coords.longitude
      //       //alert("r succ"+resp.coords.latitude)
      //       alert(JSON.stringify( resp.coords));
      
      //       // this.lat=resp.coords.latitude
      //       // this.lng=resp.coords.longitude
      //       },er=>{
      //         //alert("error getting location")
      //         alert('Can not retrieve Location')
      //       }).catch((error) => {
      //       //alert('Error getting location'+JSON.stringify(error));
      //       alert('Error getting location - '+JSON.stringify(error))
      //       });
  }

  setSearchbarText(text){
    this.searchText.value = text;
    this.autocomplete.query = text;
    this.showList = false;
  }
  profile() {
    this.navCtrl.push(RiderprofilePage);
  }
  chats() {
    this.navCtrl.push(NotificationsPage);
  }
}