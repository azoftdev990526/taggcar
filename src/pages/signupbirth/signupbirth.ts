import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
//import { SignupPage } from '../signup/signup';
import {signupaddressPage} from '../signupaddress/signupaddress';

@Component({
  selector: 'signup-birth',
  templateUrl: 'signupbirth.html'
})
export class signupbirthPage {

    navParams: NavParams
    data: {
        birthday: string
    }

    constructor(navParams: NavParams,public navCtrl: NavController, private toastCtrl: ToastController) {
        this.navParams = navParams;
        this.data = this.navParams.get('data');
    }
  
    showToast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 1500,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
      }  

    continuebirth(birth){
        // this.navCtrl.push(SignupPage);
        
        if(birth == undefined){
            this.showToast("Invalid Birthday!");
        } else{

            this.data.birthday = birth;
            this.navCtrl.push(signupaddressPage, {
                data: this.data
            });
        }
    }
    
}
