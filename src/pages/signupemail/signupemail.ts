import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import {NavParams} from "ionic-angular";
import {signupnamePage} from '../signupname/signupname';

@Component({
  selector: 'signup-email',
  templateUrl: 'signupemail.html'
})
export class signupemailPage {
  navParams: NavParams;
  email: string;
  role: string;
  constructor(navParams: NavParams, public navCtrl: NavController, private toastCtrl: ToastController) {
    this.navParams = navParams;
    this.email = this.navParams.get('email');
    this.role = this.navParams.get('role');
  }
  
  showToast(message: string) {
    let toast = this.toastCtrl.create({
        message: message,
        duration: 1500,
        position: 'bottom'
    });
    toast.onDidDismiss(() => {
        console.log('Dismissed toast');
    });
    toast.present();
  }   

    continueemail(email){
      if(email != undefined){
        if(email.indexOf('@') > 0){
          this.navCtrl.push(signupnamePage, {
              "role": this.role,
              "email": email
          });
        }
        else {
          this.showToast("Invalid Email");
        }
      }
      else {
         this.showToast("Invalid Email");
      }
    }
}
