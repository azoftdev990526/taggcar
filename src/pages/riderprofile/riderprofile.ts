import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Component, Inject } from '@angular/core';
import { App, NavController } from 'ionic-angular';

import { ConfirmpopupPage } from '../confirmpopup/confirmpopup';
//import { dashboardPage } from '../dashboard/dashboard';
import { LoginPage } from '../login/login';
import { PayPage } from "../pay/pay";
import { NotificationsPage } from "../notifications/notifications";
import { ReviewsPage } from "../reviews/reviews";
import { PasswordPage } from "../password/password";
import { RiderprofileEditPage } from '../riderprofile_edit/riderprofile';

@Component({
  selector: 'page-riderprofile',
  templateUrl: 'riderprofile.html'
})
export class RiderprofilePage {
  rideprofile: string = "about";
  avatarUrl: string = "";
  userName: string = "";
  appConfig: any;

  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController) {
    this.userName = config.info.name;
    this.avatarUrl = config.info.avatar_url;
    this.appConfig = config.info;
  }
  confirmpopup(){
    this.navCtrl.push(ConfirmpopupPage);
  } 
  logout() {
    //this.app.getRootNav().setRoot(dashboardPage);
    this.navCtrl.push(LoginPage);
    this.config.LoggedId = -1;
  }
  paymentsandrefunds(){
    this.navCtrl.push(PayPage);
  }
  notifications(){
    this.navCtrl.push(NotificationsPage);
  }
  reviews(){
    this.navCtrl.push(ReviewsPage);
  }
  password(){
    this.navCtrl.push(PasswordPage);
  }
  editprofile(){
    this.navCtrl.push(RiderprofileEditPage);
  }
}