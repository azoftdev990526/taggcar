import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Component, Inject } from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { ConfirmpopupPage } from '../confirmpopup/confirmpopup';
//import { dashboardPage } from '../dashboard/dashboard';
import { LoginPage } from '../login/login';
import { PayPage } from "../pay/pay";
import { NotificationsPage } from "../notifications/notifications";
import { ReviewsPage } from "../reviews/reviews";
import { PasswordPage } from "../password/password";
import { Constants } from '../../models/constants.models';
import { RiderprofilePage } from '../riderprofile/riderprofile';
import { UAHomePage } from '../ua_home/ua_home';

@Component({
  selector: 'page-riderprofile',
  templateUrl: 'riderprofile.html'
})
export class RiderprofileEditPage {
  rideprofile: string = "about";
  appConfig: any;


  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController,private http: HttpClient) {
    this.appConfig = config.info;
  }
  confirmpopup(){
    this.navCtrl.push(ConfirmpopupPage);
  } 
  logout() {
    //this.app.getRootNav().setRoot(dashboardPage);
    this.navCtrl.push(LoginPage);
    this.config.LoggedId = -1;
  }
  paymentsandrefunds(){
    this.navCtrl.push(PayPage);
  }
  notifications(){
    this.navCtrl.push(NotificationsPage);
  }
  reviews(){
    this.navCtrl.push(ReviewsPage);
  }
  password(){
    this.navCtrl.push(PasswordPage);
  }
  onSave(){

    var req = {
      "id" : this.appConfig.id,
      "name" : this.appConfig.name,
      "email" : this.appConfig.email,
      "phone" : this.appConfig.phone,
      "birthday" : this.appConfig.birthday,
      "car_details" : this.appConfig.car_details,
    };
    // console.log(req);
    this.http.post(Constants.API_URL + "/edit_profile", req).subscribe((res) => {
      this.config.info = res;
      this.config.info.avatar_url = Constants.API_URL + "/../assets/img/avatar/" +  this.config.info.avatar_url;
      this.appConfig = this.config.info;

      this.navCtrl.pop();
      this.navCtrl.pop();
      this.navCtrl.push(RiderprofilePage);
      // console.log(res);
      // this.dismissLoading();
      // if (res['status']==1) {
      //   this.config.LoggedId = res['info']['id'];
      //   this.config.driverData.driver_id = this.config.LoggedId;
      //   this.config.info = res['info'];
      //   this.config.info.avatar_url = Constants.API_URL + "/../assets/img/avatar/" +  this.config.info.avatar_url;
      //   this.navCtrl.push(StartPage);
      // } else {
      //   this.showToast(res['msg']);
      // }
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
      // this.dismissLoading();
    });
  }
  editAvatar(){
    this.navCtrl.push(UAHomePage);
  }
}