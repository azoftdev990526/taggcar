import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';

import {signuppasswordPage} from '../signuppassword/signuppassword';

@Component({
  selector: 'signup-address',
  templateUrl: 'signupaddress.html'
})
export class signupaddressPage {

    navParams: NavParams
    data: {
        gender: string
    }

    constructor(navParams: NavParams,public navCtrl: NavController) {
        this.navParams = navParams;
        this.data = this.navParams.get('data');
    }
  
    continueaddress(gender){
        this.data.gender = gender;
        this.navCtrl.push(signuppasswordPage, {
            data: this.data
        });
    }
    
}
