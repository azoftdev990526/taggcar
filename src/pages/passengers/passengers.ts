
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FindRidePage } from '../findride/findride';
import { dashboardPage } from '../dashboard/dashboard';

@Component({
  selector: 'page-passengers',
  templateUrl: 'passengers.html'
})
export class PassengersPage {

  numberValue:number;

  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController) {
    this.numberValue = this.config.findOptions.pcount;
  }  
  minus(){
    this.numberValue --;
    if (this.numberValue < 1)
      this.numberValue = 1;
  }
  plus(){
    this.numberValue ++;
    if(this.numberValue > 5) 
      this.numberValue = 5;
  }
  confirm() {
    this.config.findOptions.pcount = this.numberValue;
    if (this.config.LoggedId)
      this.navCtrl.setRoot(FindRidePage);
    else 
      this.navCtrl.setRoot(dashboardPage);
  }
}