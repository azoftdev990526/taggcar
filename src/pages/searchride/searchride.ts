import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { ChattingPage } from '../chatting/chatting';

@Component({
  selector: 'search-ride',
  templateUrl: 'searchride.html'
})
export class SearchridePage {

  item:any;

  constructor(public navCtrl: NavController,@Inject(APP_CONFIG) private config: AppConfig) {
    this.item = this.config.item;
  }
  
     chatting(){
      this.navCtrl.push(ChattingPage);
    }

}
