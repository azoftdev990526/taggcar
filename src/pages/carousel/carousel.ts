import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';

@Component({
  selector: 'page-carousel',
  templateUrl: 'carousel.html'
})
export class Carousel {
  // Optional parameters to pass to the swiper instance.
  // See http://idangero.us/swiper/api/ for valid options.
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
 constructor(public navCtrl: NavController) {}

 onFindYourPeople(){
    this.navCtrl.push(SignupPage);
 }

 onSignup(){
    this.navCtrl.push(SignupPage);
 }

 onLogin(){
    this.navCtrl.push(LoginPage);
 }
}