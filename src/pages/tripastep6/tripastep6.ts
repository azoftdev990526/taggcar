import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { TripastepPage } from '../tripastep/tripastep';
import { RiderprofilePage } from '../riderprofile/riderprofile';
import { NotificationsPage } from '../notifications/notifications';

@Component({
  selector: 'page-tripastep6',
  templateUrl: 'tripastep6.html'
})
export class Tripastep6Page {
  rideprofile: string = "about";
  price:number;

  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController) {
    this.price = this.config.driverData.price;
  }
  continue(price){
    this.config.driverData.price = price;
    this.navCtrl.push(TripastepPage);
  }
  continueno() {
    document.getElementById('price').style.display = "none";
    document.getElementById('inputprice').style.display = "block";
  }
  profile() {
    this.navCtrl.push(RiderprofilePage);
  }
  chats() {
    this.navCtrl.push(NotificationsPage);
  }

}