import { Component, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Constants } from '../../models/constants.models';


// @IonicPage()
@Component({
  selector: 'page-identifyphoto',
  templateUrl: 'identifyimage.html',
})
export class IdentifyphotoPage {
  base64img: string = '';
  constructor(public loadingCtrl: LoadingController,@Inject(APP_CONFIG) private config: AppConfig,  public nav: NavController, private transfer: FileTransfer) {
    this.base64img = this.config.base64img;
  }

  uploadPic() {
    let loader = this.loadingCtrl.create({
      content: "Uploading...."
    });
    loader.present();

    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: "photo",
      fileName: "test3.jpg",
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }

    fileTransfer.upload(this.base64img, Constants.API_URL + "/uploadavatar", options).then(data => {
      alert(JSON.stringify(data));
      loader.dismiss();
    }, error => {
      alert("error");
      alert("error" + JSON.stringify(error));
      loader.dismiss();
    });
  }

}