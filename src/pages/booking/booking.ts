
import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { PayPage } from '../pay/pay';
import { MainPage } from '../main/main';

@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html'
})
export class BookingPage {

  item: any;

  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController) {
    this.item = this.config.item;
  }
  
  book(){
    if (this.config.LoggedId)
      this.navCtrl.push(PayPage);
    else 
      this.navCtrl.setRoot(MainPage);
  }

}
