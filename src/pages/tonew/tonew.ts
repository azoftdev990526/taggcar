import { Component, Inject } from '@angular/core';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { FindRidePage } from '../findride/findride';
import { dashboardPage } from '../dashboard/dashboard';
import { NavParams} from "ionic-angular";
import { LocationPage } from '../location/location';
import { Constants } from "../../models/constants.models";

@Component({
  selector: 'to-where',
  templateUrl: 'tonew.html'
})
export class tonewPage {
  to: string;
  to_cityId: number;
  to_placeId: number;
  to_lat: number;
  to_lng: number;
  items: any;
  showList:boolean = false;
  
  constructor(@Inject(APP_CONFIG) private config: AppConfig, private http: HttpClient, public navCtrl: NavController,navParams: NavParams) {
    this.to = this.config.findOptions.to;
  }
  searchItems(searchTxt) {
    this.showList = false;
    this.http.get(Constants.API_URL + "/getLocation?keyword="+searchTxt).subscribe((res) => {
      this.items = res;
      this.showList = true;
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
    });
  }
  getItems(ev: any) {
    this.showList = false;
    let val = ev.target.value;
    if (val=='') {
      this.items = [];
    } else {
      this.searchItems(val);
    }    
  }
  selectItem(item) {
    this.to = item.location;
    this.to_cityId = item.city_id;
    this.to_placeId = item.id;
    this.to_lat = item.lat;
    this.to_lng = item.lng;
    this.showList = false;
  }
  confirm() {
    this.config.findOptions.to = this.to;
    this.config.findOptions.to_cityId = this.to_cityId;
    this.config.findOptions.to_placeId = this.to_placeId;
    this.config.findOptions.to_lat = this.to_lat;
    this.config.findOptions.to_lng = this.to_lng;

    if (this.config.LoggedId)
      this.navCtrl.setRoot(FindRidePage);
    else 
      this.navCtrl.setRoot(dashboardPage);
  }
  currentlocation() {
    this.navCtrl.push(LocationPage) 
  }
}
