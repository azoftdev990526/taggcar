import { AfterViewChecked, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { App, Content, LoadingController, NavController } from 'ionic-angular';

import { ConfirmpopupPage } from '../confirmpopup/confirmpopup';
import { MainPage } from '../main/main';
import { ChattingPage } from '../chatting/chatting';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Constants } from '../../models/constants.models';
import { HttpClient } from '@angular/common/http';
import { NotificationsPage } from '../notifications/notifications';
@Component({
  selector: 'page-mychatting',
  templateUrl: 'mychatting.html'
})
export class MychattingPage implements OnInit , AfterViewChecked {
  rideprofile: string = "about";
  avatarPrefix : any;
  msgList: any;
  loading: any;
  loadingShown: boolean;
  myid: any;
  opp: any;
  msgSending: any;
  isScroll : any;
  
  instance: MychattingPage;

  @ViewChild('content') private content: Content;

  constructor(@Inject(APP_CONFIG) private config: AppConfig,public navCtrl: NavController,private app: App, private loadingCtrl: LoadingController  ,private http: HttpClient) {
    this.myid = this.config.LoggedId;
    this.opp = this.config.opp;
    this.msgSending = "";
    this.isScroll = true;
    this.avatarPrefix = Constants.API_URL + "/../assets/img/avatar/";
    this.instance = this;
  }
  ngAfterViewChecked(): void {
    // throw new Error('Method not implemented.');
  }
  ngOnInit(): void {

    this.presentLoading("Loading ...");

    this.http.post(Constants.API_URL + "/getMessages", {
      "myid" : this.config.LoggedId,
      "oppid" : this.config.opp.id,
    }).subscribe((res) => {
      this.msgList = res;
      this.dismissLoading();
      NotificationsPage.instance.ngOnInit();
      this.scrollBottom();
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
      this.dismissLoading();
    });

    setInterval(()=>{
      this.http.post(Constants.API_URL + "/getMessages", {
        "myid" : this.config.LoggedId,
        "oppid" : this.config.opp.id,
      }).subscribe((res) => {
        this.msgList = res;
        this.scrollBottom();
      }, (err) => {
        console.log("Error Network:", JSON.stringify(err));
      });
    }, 1000);
  }

  scrollBottom(){
    setTimeout(()=>{
      if(this.isScroll == true){
        this.content.scrollTo(0, this.content.getContentDimensions().scrollHeight , 0);
        this.isScroll = false;
      }
      else if(this.content.getContentDimensions().scrollTop + this.content.getContentDimensions().contentHeight >= this.content.getContentDimensions().scrollHeight - 200){
        this.content.scrollTo(0, this.content.getContentDimensions().scrollHeight , 0);
      }
    }, 0);
  }

  presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.onDidDismiss(() => { });
    this.loading.present();
    this.loadingShown = true;
  }

  dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }

  confirmpopup(){
    this.navCtrl.push(ConfirmpopupPage);
  } 
  logout() {
    //this.navCtrl.setRoot(MainPage);
    this.app.getRootNav().setRoot(MainPage)
  }
  chatting(){
    this.navCtrl.push(ChattingPage);
  }

  onSend(){
    this.http.post(Constants.API_URL + "/messageSave", {
      "sender" : this.config.LoggedId,
      "receiver" : this.config.opp.id,
      "content" : this.msgSending,
    }).subscribe((res) => {
      this.msgSending = "";
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
    });
  }

}