import { Component, Inject } from '@angular/core';
import { NavController,LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { TripPage } from '../trip/trip';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Constants } from "../../models/constants.models";

@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
  items: any;
  search: any = {
    from: '',
    from_cityId: 0,
    from_placeId: 0,
    from_lat: 0,
    from_lng: 0,
    to : '',
    to_cityId: 0,
    to_placeId: 0,
    to_lat: 0,
    to_lng: 0,
    
    pcount: 1,
    pdate: '',
    nDate: 1,
    nWeek: 1,
    nMonth: 1,
  };
  loading: any;
  loadingShown: boolean;

  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController, private http: HttpClient, private loadingCtrl: LoadingController) {
    if (this.config.findOptions.from!='')
      this.search = this.config.findOptions;

    let date = new Date(this.search.pdate);
    this.search.nDate = date.getUTCDate();
    this.search.nWeek = date.getUTCDay();
    this.search.nMonth = date.getUTCMonth();
  }
  ngOnInit() {
    this.presentLoading('Loading...');
    let req = {
      "departure_id": this.search.from_cityId,
      "departure" : this.search.from,
      "destination_id": this.search.to_cityId, 
      "destination" : this.search.to,
      "passengers": this.search.pcount, 
      "date": this.search.pdate 
    }
    this.http.post(Constants.API_URL + "/search", req).subscribe((res) => {
      this.dismissLoading();
        this.dismissLoading();
        console.log(req);
        
        this.items = res;
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
      this.dismissLoading();
    });
  }
  presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.onDidDismiss(() => { });
    this.loading.present();
    this.loadingShown = true;
  }

  dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }
  Trip(item){
    this.config.item = item;
    this.navCtrl.push(TripPage);
  }
}
