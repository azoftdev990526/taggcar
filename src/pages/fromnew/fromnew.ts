import { Component, Inject } from '@angular/core';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { FindRidePage } from '../findride/findride';
import { dashboardPage } from '../dashboard/dashboard';
import { NavParams} from "ionic-angular";
import { LocationPage } from '../location/location';
import { Constants } from "../../models/constants.models";


@Component({
  selector: 'from-where',
  templateUrl: 'fromnew.html'
})
export class fromnewPage {
  from: string;
  from_cityId: number;
  from_placeId: number;
  from_lat: number;
  from_lng: number;
  items: any;
  showList:boolean = false;
  
  constructor(@Inject(APP_CONFIG) private config: AppConfig, private http: HttpClient, public navCtrl: NavController,navParams: NavParams) {
    this.from = this.config.findOptions.from;
  }
  searchItems(searchTxt) {
    this.showList = false;
    this.http.get(Constants.API_URL + "/getLocation?keyword="+searchTxt).subscribe((res) => {
      this.items = res;
      this.showList = true;
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
    });
  }
  getItems(ev: any) {
    this.showList = false;
    let val = ev.target.value;
    if (val=='') {
      this.items = [];
    } else {
      this.searchItems(val);
    }    
  }
  selectItem(item) {
    this.from = item.location;
    this.from_cityId = item.city_id;
    this.from_placeId = item.id;
    this.from_lat = item.lat;
    this.from_lng = item.lng;
    this.showList = false;
  }
  confirm() {
    this.config.findOptions.from = this.from;
    this.config.findOptions.from_cityId = this.from_cityId;
    this.config.findOptions.from_placeId = this.from_placeId;
    this.config.findOptions.from_lat = this.from_lat;
    this.config.findOptions.from_lng = this.from_lng;
    if (this.config.LoggedId)
      this.navCtrl.setRoot(FindRidePage);
    else 
      this.navCtrl.setRoot(dashboardPage);
  }
  currentlocation() {
    this.navCtrl.push(LocationPage) 
  }
}
