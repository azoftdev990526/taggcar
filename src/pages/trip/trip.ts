import { Component, Inject } from '@angular/core';
import { NavController,LoadingController } from 'ionic-angular';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { SearchridePage } from '../searchride/searchride';
import { BookingPage } from '../booking/booking';
import { HttpClient } from '@angular/common/http';
import { LocationViewPage } from "../location_view/location_view";
import { NotificationsPage } from '../notifications/notifications';
import { Constants } from '../../models/constants.models';

@Component({
  selector: 'page-trip',
  templateUrl: 'trip.html'
})
export class TripPage {

  item:any;
  detail:any;
  formated_date:any;
  nDate:number=1;
  nWeek:number=1;
  nMonth:number=1;
  nYear:number=2020;

  loading: any;
  loadingShown: boolean;

  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController, private http: HttpClient, private loadingCtrl: LoadingController) {
    this.item = this.config.item;
    let date = new Date(this.item.start_date);
    this.nDate = date.getUTCDate();
    this.nWeek = date.getUTCDay();
    this.nMonth = date.getUTCMonth();
    this.nYear = date.getUTCFullYear();
  }
  
  ngOnInit() {
    console.log(this.item);
    // this.presentLoading('Loading...');
    // let req = {
    //   "path_detail": this.item.path_detail,

    //   "from_cityId": this.config.findOptions.from_cityId, 
    //   "from_placeId": this.config.findOptions.from_placeId,
    //   "from_lat": this.config.findOptions.from_lat,
    //   "from_lng": this.config.findOptions.from_lng,

    //   "to_cityId": this.config.findOptions.to_cityId, 
    //   "to_placeId": this.config.findOptions.to_placeId,
    //   "to_lat": this.config.findOptions.to_lat,
    //   "to_lng": this.config.findOptions.to_lng,
    // }
    // this.http.post(Constants.API_URL + "/trip_detail", req).subscribe((res) => {
    //   this.dismissLoading();
    //     this.detail = res;
    //     console.log(this.detail);
    // }, (err) => {
    //   console.log("Error Network:", JSON.stringify(err));
    //   this.dismissLoading();
    // });
  }
  presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.onDidDismiss(() => { });
    this.loading.present();
    this.loadingShown = true;
  }

  dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }
  driver_profile(){
    this.navCtrl.push(SearchridePage);
  }
  contact_driver(){
    console.log("~~item:");
    console.log(this.item);
    console.log(this.config);

    this.http.post(Constants.API_URL + "/addAvailable", {
      "owner" : this.item.driver_id,
      "follower" : this.config.LoggedId,
    }).subscribe((res) => {
      this.navCtrl.push(NotificationsPage);
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
    });
  }
  booking(){
    this.navCtrl.push(BookingPage)
  }

  onDeparture(){
    this.config.driverData.arrival_latlng = {
      "lat": parseFloat(this.item.from_lat),
      "lng": parseFloat(this.item.from_lng),
    }
    this.config.driverData.departure = this.item.departure;
    this.navCtrl.push(LocationViewPage);
  }

  onArrival(){
    this.config.driverData.arrival_latlng = {
      "lat": parseFloat(this.item.to_lat),
      "lng": parseFloat(this.item.to_lng),
    }
    this.config.driverData.departure = this.item.arrival;
    this.navCtrl.push(LocationViewPage);
  }
}
