import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';

import {signupnumberPage} from '../signupnumber/signupnumber';

@Component({
  selector: 'signup-password',
  templateUrl: 'signuppassword.html'
})
export class signuppasswordPage {

    navParams: NavParams
    data: {
        password: ""
    }

    constructor(navParams: NavParams,public navCtrl: NavController, private toastCtrl: ToastController) {
        this.navParams = navParams;
        this.data = this.navParams.get('data');
    }
  
    showToast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 1500,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
      }   

    continuenumber(password){
        this.data.password = password;
        console.log(password);
        if(password == undefined || ("" + password).length < 8){
            this.showToast("Password Weak!");
        } else{
            this.navCtrl.push(signupnumberPage, {
                data: this.data
            });
        }
    }
    

}
