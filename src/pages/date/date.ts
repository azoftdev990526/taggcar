import { Component, Inject } from '@angular/core';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { NavController } from 'ionic-angular';
import { FindRidePage } from '../findride/findride';
import { dashboardPage } from '../dashboard/dashboard';

@Component({
  selector: 'date-when',
  templateUrl: 'date.html'
})
export class datePage {
  pdate: string;
  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController) {
    this.pdate = this.config.findOptions.pdate;
  }
  confirm() {
    this.config.findOptions.pdate = this.pdate;
    if (this.config.LoggedId)
      this.navCtrl.setRoot(FindRidePage);
    else 
      this.navCtrl.setRoot(dashboardPage);
  }

}
