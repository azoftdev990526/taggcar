import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { VerificationPage } from '../verification/verification';
import { DonePage } from '../done/done';

@Component({
  selector: 'page-pay',
  templateUrl: 'pay.html'
})
export class PayPage {

    visiblecard = false;
    visiblepay = false;
    months = ["MM","01","02","03","04","05","06","07","08","09","10","11","12"];
    years = ["YYYY","2018","2019","2020","2021","2022","2023","2024"];
    constructor(public navCtrl: NavController) {

    }
  
    login(){
        this.navCtrl.push(LoginPage);
    }
     
    verification(){
        this.navCtrl.push(VerificationPage);
    }

    toggleCard() {
        this.visiblecard = !this.visiblecard;
    }
    togglePayPal() {
        this.visiblepay = !this.visiblepay;
    }

    payPaypal(){
        this.navCtrl.push(DonePage);
    }

    payCard(){
        this.navCtrl.popToRoot();
        this.navCtrl.push(DonePage);
    }
}
