import { Component, Inject } from '@angular/core';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { NavController, ModalController, LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { fromnewPage } from '../fromnew/fromnew';
import { tonewPage } from '../tonew/tonew';
import { datePage } from '../date/date';
import { SearchPage } from '../search/search';
import { PassengersPage } from '../passengers/passengers';
import { TripPage } from '../trip/trip';
import { MainPage } from '../main/main';
import { SignupPage } from '../signup/signup';
import { LoginPage } from '../login/login';
import { RiderprofilePage } from '../riderprofile/riderprofile';

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class dashboardPage {
  items: any;
  ride: string = "Upconing";
  from: string;
  to: string;
  pdate:string;
  pcount:number;
  loading: any;
  loadingShown: boolean;

  constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController, private http: HttpClient, private loadingCtrl: LoadingController, public modalCtrl: ModalController) {
    this.from = this.config.findOptions.from;
    this.to = this.config.findOptions.to;
    this.pdate = this.config.findOptions.pdate;
    this.pcount = this.config.findOptions.pcount;
  }

  ngOnInit() {
    this.presentLoading('Loading...');
    let req = {
      "departure": "",
      "destination": "", 
      "passengers": 0, 
      "date": this.pdate,
      "max":3
    }
    this.http.post("/api/search", req).subscribe((res) => {
      this.dismissLoading();
        this.dismissLoading();
        this.items = res;
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
      this.dismissLoading();
    });

    this.navCtrl.push(LoginPage);
  }
  presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.onDidDismiss(() => { });
    this.loading.present();
    this.loadingShown = true;
  }

  dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }

  fromwhere() {
    this.navCtrl.push(fromnewPage);
  }
  towhere() {
    this.navCtrl.push(tonewPage);
  }
  search() {
    if (this.config.findOptions.from_cityId>0 && this.config.findOptions.to_cityId>0)
      this.navCtrl.push(SearchPage);
  }
  passengers() {
    this.navCtrl.push(PassengersPage);
  }
  date() {
    this.navCtrl.push(datePage);
  }
  profile() {
    this.navCtrl.push(RiderprofilePage);
  }
  login() {
    this.navCtrl.push(MainPage);
  }
  signup() {
    this.navCtrl.push(SignupPage);
  }
  swap() {
    this.config.findOptions.from= this.to;
    this.config.findOptions.to  = this.from;
    this.from= this.config.findOptions.from;
    this.to  = this.config.findOptions.to;
  }
  Trip(item){
    this.config.item = item;
    this.navCtrl.push(TripPage);
  }
}

