import { Component, Inject } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { RateriderPage } from '../raterider/raterider';
import { RidetodayPage } from '../ridetoday/ridetoday';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { BuyAppAlertPage } from '../buyappalert/buyappalert';
import { NotificationsPage } from '../notifications/notifications';
import { RiderprofilePage } from '../riderprofile/riderprofile';
import { Constants } from "../../models/constants.models";
import { MyRideDetailPage } from '../myride_detail/myride_detail';

@Component({
  selector: 'page-myride',
  templateUrl: 'myride.html'
})
export class MyridePage {

  public static Instance: MyridePage = null;
  ride: string = "today";
  loading: any;
  loadingShown: boolean;
  myRides: any;
  avatarUrl: string = "";

  constructor(@Inject(APP_CONFIG) public config: AppConfig, public navCtrl: NavController, public modalCtrl: ModalController, private http: HttpClient, private loadingCtrl: LoadingController) {
    MyridePage.Instance = this;
    this.avatarUrl = config.info.avatar_url;
  }

  ngOnInit(){
    this.presentLoading('Loading...');
    let req = {
      "customer_id" : this.config.LoggedId,
    }
    this.http.post(Constants.API_URL + "/getMyRide", req).subscribe((res) => {
      this.dismissLoading();
        this.dismissLoading();
        console.log(res);
        this.myRides = res;
        //this.items = res;
    }, (err) => {
      console.log("Error Network:", JSON.stringify(err));
      this.dismissLoading();
    });
  }

  presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.onDidDismiss(() => { });
    this.loading.present();
    this.loadingShown = true;
  }
  dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }
  raterider() {
    this.navCtrl.push(RateriderPage);
  }

  profile() {
    this.navCtrl.push(RiderprofilePage);
  }
  chats() {
    this.navCtrl.push(NotificationsPage);
  }
  ridetoday() {
    this.navCtrl.push(RidetodayPage);
  }
  buyThisApp() {
    let profileModal = this.modalCtrl.create(BuyAppAlertPage, { userId: 8675309 });
    profileModal.present();
  }

  onItem(item){
    this.config.item = item;
    this.navCtrl.push(MyRideDetailPage);
  }
}
