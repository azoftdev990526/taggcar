import { NavParams } from "ionic-angular";
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Tripastep4Page } from '../tripastep4/tripastep4';
import { RiderprofilePage } from '../riderprofile/riderprofile';
import { NotificationsPage } from '../notifications/notifications';


@Component({
  selector: 'page-tripastep3',
  templateUrl: 'tripastep3.html'
})
export class Tripastep3Page {

  startdate: string;

  constructor(@Inject(APP_CONFIG) private config: AppConfig,navParams: NavParams,public navCtrl: NavController) {
    let today = new Date();
    this.startdate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  }
  continuestep4() {
    this.config.driverData.start_date = this.startdate;
    this.navCtrl.push(Tripastep4Page);
  }
  profile() {
    this.navCtrl.push(RiderprofilePage);
  }
  chats() {
    this.navCtrl.push(NotificationsPage);
  }
}