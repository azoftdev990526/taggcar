import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {NavParams} from "ionic-angular";
import {signupbirthPage} from '../signupbirth/signupbirth';

@Component({
  selector: 'signup-name',
  templateUrl: 'signupname.html'
})
export class signupnamePage {

    email: string
    role: string
    navParams: NavParams

    constructor(navParams: NavParams,public navCtrl: NavController) {
        this.navParams = navParams;
        this.email = this.navParams.get('email');
        this.role = this.navParams.get('role');
    }
  
    continuename(firstname, lastname){
        this.navCtrl.push(signupbirthPage, {
            "data": {
                "email": this.email,
                "role": this.role,
                "firstname": firstname,
                "lastname": lastname
            }
        });
    }
}
