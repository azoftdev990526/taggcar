import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';


import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { AppConfig, APP_CONFIG } from '../../app/app.config';

@Component({
     selector: 'page-message',
     templateUrl: 'message.html'
})
export class MessagePage {

     constructor(@Inject(APP_CONFIG) private config: AppConfig, public navCtrl: NavController, private inAppBrowser: InAppBrowser) {

     }

     developedBy() {
          const options: InAppBrowserOptions = {
               zoom: 'no'
          }
          this.inAppBrowser.create('https://verbosetechlabs.com/', '_system', options);
     }
}
