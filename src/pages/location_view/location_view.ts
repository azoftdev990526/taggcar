import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Platform } from "ionic-angular";
import { Geolocation } from '@ionic-native/geolocation';
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { ChangeDetectorRef } from "@angular/core";
import { Tripastep3Page } from '../tripastep3/tripastep3';

declare var google: any;
@Component({
  selector: 'page-location-view',
  templateUrl: 'location_view.html'
})
export class LocationViewPage {
  map:any;
  geocoder: any;
  markers: any;
  currentMarker: any;
  sub_title:string;
  latlng: any;

  constructor(@Inject(APP_CONFIG) private config: AppConfig,private changeDetectorRef:ChangeDetectorRef, public navCtrl: NavController, public platform: Platform, public geolocation: Geolocation) {
    this.sub_title=this.config.driverData.departure;
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => this.loadMap());
	}

  loadMap() {
		/* The create() function will take the ID of your map element */
	
      //Set latitude and longitude of some place
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: this.config.driverData.arrival_latlng,
        zoom: 15
      });


      this.geocoder = new google.maps.Geocoder;
      this.markers = [];

      // this.geolocation.getCurrentPosition().then((resp) => {

      //   let pos = {
      //     lat: resp.coords.latitude,
      //     lng: resp.coords.longitude
      //   };

      //   // new google.maps.Geocoder().geocode({'location' : pos}, function(results, status) {
      //   //   console.log(results[0].formatted_address);
            
      //   // }); 
      //   // this.currentMarker = new google.maps.Marker({
      //   //   position: pos,
      //   //   map: this.map,
      //   //   title: 'I am here!'
      //   // });
      //  // this.markers.push(marker);
      //   this.map.setCenter(pos);
      // }).catch((error) => {
      //   console.log('Error getting location', error);
      // });

      var vMap = this.map;
      var vMarker = this.currentMarker;
      let vThis = this;
      google.maps.event.addListener(this.map, "click", function (e) {
        // console.log(e);
        // latLng = e.latLng;
      
        // console.log(e.latLng.lat());
        // console.log(e.latLng.lng());
      
        // image = clientURL + "/common/images/markers/red.png";
      
        // if marker exists and has a .setMap method, hide it
        if (vMarker && vMarker.setMap) {
          // console.log("Marker");
          vMarker.setMap(null);
        }
        vMarker = new google.maps.Marker({
          position: e.latLng,
          map: vMap,
        });
      
        new google.maps.Geocoder().geocode({'location' : e.latLng}, function(results, status) {
          console.log(results[0].formatted_address);
          vThis.setSubTitle(results[0].formatted_address);
          vThis.latlng = e.latLng;
        }); 
      });
  }
  
  setSubTitle(text){
    this.sub_title = text;
    this.changeDetectorRef.detectChanges();
  }

  continue(){
    this.config.driverData.arrival_latlng = this.latlng;
    this.config.driverData.arrival_dropoff = this.sub_title;
    this.navCtrl.push(Tripastep3Page);
  }
}
