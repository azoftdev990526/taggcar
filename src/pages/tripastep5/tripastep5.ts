
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Tripastep6Page } from '../tripastep6/tripastep6';
import { RiderprofilePage } from '../riderprofile/riderprofile';
import { NotificationsPage } from '../notifications/notifications';
import { ChangeDetectorRef } from "@angular/core";

@Component({
  selector: 'page-tripastep5',
  templateUrl: 'tripastep5.html'
})
export class Tripastep5Page {

  rideprofile:string = "about";
  numberValue:number;

  constructor(@Inject(APP_CONFIG) private config: AppConfig,private changeDetectorRef:ChangeDetectorRef, public navCtrl: NavController) {
    this.numberValue = this.config.findOptions.pcount;
  }  
  minus(){
    this.numberValue --;
    if (this.numberValue < 1)
      this.numberValue = 1;

    this.changeDetectorRef.detectChanges();
  }
  plus(){
    this.numberValue ++;
    if(this.numberValue > 5) 
      this.numberValue = 5;
    this.changeDetectorRef.detectChanges();
  }
  continuestep6() {
    this.config.driverData.passengers = this.numberValue;
    this.navCtrl.push(Tripastep6Page);
  }
  profile() {
    this.navCtrl.push(RiderprofilePage);
  }
  chats() {
    this.navCtrl.push(NotificationsPage);
  }
}