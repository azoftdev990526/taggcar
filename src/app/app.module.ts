import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { MyridePage } from '../pages/myride/myride';
import { fromPage } from '../pages/from/from';
import { fromnewPage, } from '../pages/fromnew/fromnew';
import { toPage } from '../pages/to/to';
import { tonewPage} from '../pages/tonew/tonew';
import { datePage } from '../pages/date/date';
import { timePage } from '../pages/time/time';
import { ChatsPage } from '../pages/chats/chats';
import { EditRidePage } from '../pages/editride/editride';
import { WalletPage } from '../pages/wallet/wallet';
import { MorePage } from '../pages/more/more';
import { LoginPage } from '../pages/login/login';
import { MainPage } from '../pages/main/main';
import { StartPage } from '../pages/start/start';
import { FindRidePage } from '../pages/findride/findride';
import { SignupPage } from '../pages/signup/signup';
import { PayPage } from '../pages/pay/pay';
import { PasswordPage } from '../pages/password/password';
import { VerificationPage } from '../pages/verification/verification';
import { CodePage } from '../pages/code/code';
import { ListridePage } from '../pages/listride/listride';
import { FilterPage } from '../pages/filter/filter';
import { RiderprofilePage } from '../pages/riderprofile/riderprofile';
import { ConfirmridePage } from '../pages/confirmride/confirmride';
import { ConfirmpopupPage } from '../pages/confirmpopup/confirmpopup';
import { RateriderPage } from '../pages/raterider/raterider';
import { ChattingPage } from '../pages/chatting/chatting';
import { ProfilePage } from '../pages/profile/profile';
import { ReviewsPage } from '../pages/reviews/reviews';
import { NotificationPage } from '../pages/notification/notification';
import { TermsPage } from '../pages/terms/terms';
import { EarnPage } from '../pages/earn/earn';
import { RatevroomPage } from '../pages/ratevroom/ratevroom';
import { HelpPage } from '../pages/help/help';
import { RidetodayPage } from '../pages/ridetoday/ridetoday';
import { UploadPage } from '../pages/upload/upload';
import { Tripastep3Page } from '../pages/tripastep3/tripastep3';
import { Tripastep4Page } from '../pages/tripastep4/tripastep4';
import { Tripastep5Page } from '../pages/tripastep5/tripastep5';
import { Tripastep6Page } from '../pages/tripastep6/tripastep6';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { APP_CONFIG, BaseAppConfig } from "./app.config";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule,HttpClient } from '@angular/common/http';
import { Globalization } from '@ionic-native/globalization';
import { ManagelanguagePage } from '../pages/managelanguage/managelanguage';
import { BuyAppAlertPage } from '../pages/buyappalert/buyappalert';
import { Vt_popupPage } from '../pages/vt_popup/vt_popup';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SearchridePage } from '../pages/searchride/searchride';
import { SearchPage } from '../pages/search/search';
import { PassengersPage } from '../pages/passengers/passengers';
import { TripPage } from '../pages/trip/trip';
import { BookingPage } from '../pages/booking/booking';
import { Signuprole } from '../pages/signuprole/signuprole';
import { signupemailPage } from '../pages/signupemail/signupemail';
import { signupnamePage } from '../pages/signupname/signupname';
import { signupbirthPage } from '../pages/signupbirth/signupbirth';
import { signupaddressPage } from '../pages/signupaddress/signupaddress';
import { signuppasswordPage } from '../pages/signuppassword/signuppassword';
import { signupnumberPage } from '../pages/signupnumber/signupnumber';
import { TripastepPage } from '../pages/tripastep/tripastep';
import { NotificationsPage } from '../pages/notifications/notifications';
import { LocationPage } from '../pages/location/location';
import { MessagePage } from '../pages/message/message';
import { MyPage } from '../pages/mypage/page';
import { MychattingPage } from '../pages/mychatting/mychatting';
import { dashboardPage } from '../pages/dashboard/dashboard';
import { AutocompletePage } from '../pages/from/AutocompletePage';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { MyRideDetailPage } from '../pages/myride_detail/myride_detail';
import { LocationViewPage } from '../pages/location_view/location_view';
import { DonePage } from '../pages/done/done';
import { RiderprofileEditPage } from '../pages/riderprofile_edit/riderprofile';
import { Carousel } from '../pages/carousel/carousel';

import { Camera } from '@ionic-native/camera/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { IdentifyphotoPage } from '../pages/ua_identifyimage/identifyimage';
import { UAHomePage } from '../pages/ua_home/ua_home';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    MyridePage,
    fromPage,
    fromnewPage,
    toPage,
    tonewPage,
    datePage,
    timePage,
    signupemailPage,
    Signuprole,
    signupnamePage,
    signupaddressPage,
    signupbirthPage,
    signuppasswordPage,
    signupnumberPage,
    ChatsPage,
    EditRidePage,
    StartPage,
    WalletPage,
    MorePage,
    LoginPage,
    MainPage,
    FindRidePage,
    PasswordPage,
    SignupPage,
    PayPage,
    SearchridePage,
    SearchPage,
    PassengersPage,
    TripPage,
    Tripastep3Page,
    Tripastep4Page,
    Tripastep5Page,
    TripastepPage,
    Tripastep6Page,
    BookingPage,
    VerificationPage,
    CodePage,
    ListridePage,
    FilterPage,
    RiderprofilePage,
    ConfirmridePage,
    ConfirmpopupPage,
    RateriderPage,
    ChattingPage,
    ProfilePage,
    ReviewsPage,
    NotificationPage,
    TermsPage,
    EarnPage,
    RatevroomPage,
    RatevroomPage,
    HelpPage,
    RidetodayPage,
    UploadPage,
    ManagelanguagePage,
    BuyAppAlertPage,
    NotificationsPage,
    Vt_popupPage,
    MessagePage,
    MyPage,
    MychattingPage,
    LocationPage,
    dashboardPage,
    AutocompletePage,
    MyRideDetailPage,
    LocationViewPage,
    DonePage,
    RiderprofileEditPage,
    Carousel,
    IdentifyphotoPage,
    UAHomePage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    MyridePage,
    fromPage,
    fromnewPage,
    tonewPage,
    toPage,
    PayPage,
    datePage,
    timePage,
    Signuprole,
    signupemailPage,
    signupnamePage,
    signupaddressPage,
    signupbirthPage,
    signuppasswordPage,
    signupnumberPage,
    ChatsPage,
    EditRidePage,
    WalletPage,
    MorePage,
    LoginPage,
    MainPage,
    TripPage,
    TripastepPage,
    Tripastep3Page,
    Tripastep4Page,
    Tripastep5Page,
    Tripastep6Page,
    FindRidePage,
    BookingPage,
    PasswordPage,
    SignupPage,
    StartPage,
    SearchridePage,
    SearchPage,
    PassengersPage,
    VerificationPage,
    CodePage,
    ListridePage,
    FilterPage,
    RiderprofilePage,
    ConfirmridePage,
    ConfirmpopupPage,
    RateriderPage,
    ChattingPage,
    ProfilePage,
    ReviewsPage,
    NotificationPage,
    TermsPage,
    EarnPage,
    RatevroomPage,
    RatevroomPage,
    HelpPage,
    RidetodayPage,
    UploadPage,
    ManagelanguagePage,
    BuyAppAlertPage,
    Vt_popupPage,
    LocationPage,
    MessagePage,
    MyPage,
    MychattingPage,
    NotificationsPage,
    dashboardPage,
    AutocompletePage,
    MyRideDetailPage,
    LocationViewPage,
    DonePage,
    RiderprofileEditPage,
    Carousel,
    IdentifyphotoPage,
    UAHomePage,
  ],
  providers: [
    Camera, 
    // File,
    FileTransfer,
    GoogleMaps,
    Geolocation,
    SplashScreen,
    StatusBar,
    Globalization,InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: APP_CONFIG, useValue: BaseAppConfig }
  ]
})
export class AppModule {}
