import { InjectionToken } from "@angular/core";

export let APP_CONFIG = new InjectionToken<AppConfig>("app.config");


export interface AppConfig {
  availableLanguages: Array<any>;
  demoMode: boolean;
  LoggedId: number;
  findOptions: any;
  driverData: any;
  item: any;
  info: any;
  opp: any;
  base64img: string;
}

export const BaseAppConfig: AppConfig = {
    availableLanguages: [{
    code: 'en',
    name: 'English'
  }, {
    code: 'ar',
    name: 'Arabic'
  }, {
    code: 'es',
    name: 'Spanish'
  }],
  LoggedId: 0,
  demoMode: true,
  findOptions: {
    from: '',
    from_cityId: 1,
    from_placeId: 1,
    from_lat: 0.1,
    from_lng: 0.1,
    to : '',
    to_cityId: 2,
    to_placeId: 2,
    to_lat: 1.1,
    to_lng: 1.2,
    pcount: 2,
    pdate: '2020-01-01'
  },
  driverData: {
    departure : '',
    arrival : '',
    path_detail: '',
    start_date : '',
    leave_time : '',
    passengers : 0,
    price : 17,
    comment : "",
    driver_id : 1,
    departure_latlng:{
      lat: Number,
      lng: Number,
    },
    arrival_latlng : {
      lat: Number,
      lng: Number,
    },
    arrival_dropoff:"",
  },
  item: {
    id:0,
    arrive_time: '',
    leave_time: '',
    arrival: '',
    departure: '',
    price: '',
    name: '',
    start_date: '',
    age: '',
    avatar_url: ''
  },
  info: {
    id: 0,
    role: "D",
    user_id: "driver",
    name: "Jerry P.",
    email: "Barcelona@gmail.com",
    gender: "Mr",
    phone: "11111111",
    birthday: "1980-08-01",
    age: 38,
    avatar_url: "man1.png",
    experience: "experienced",
    net_friends: 145,
    rating: 5,
    is_allow: "Y",
    created_at: "",
    updated_at: "",
  },
  opp: {
    id: 0,
    role: "D",
    user_id: "driver",
    name: "Jerry P.",
    email: "Barcelona@gmail.com",
    gender: "Mr",
    phone: "11111111",
    birthday: "1980-08-01",
    age: 38,
    avatar_url: "man1.png",
    experience: "experienced",
    net_friends: 145,
    rating: 5,
    is_allow: "Y",
    created_at: "",
    updated_at: "",
  },
  base64img: "",
};